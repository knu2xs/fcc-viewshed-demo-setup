'''
Name:        fccCellTowerViewshed
Purpose:     This Python toolbox facilitates downloading and creating a
             usable dataset for demonstrating the functionality of the
             viewshed tool. The data is downloaded directly from the FCC.
             It is a dataset of all the FCC cell towers in the United
             States.

Author:      Joel McCune (joelmccune@gmail.com)

Created:     16Sep2013
Copyright:   (c) Joel McCune
Licence:
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
'''
# import modules
import arcpy
import random
import os.path
import re

# assumed range for CDMA and GSM bands, respectively
g_cdmaDist = 8 # in miles
g_gsmDist = 4 # in miles

# default name for cell tower feature class
cellTowersFc = 'cellTowers'

# get directory where this script is located
localDir = os.path.dirname(os.path.realpath(__file__))

class Panel(object):
    '''
    A cell phone panel. I do not know much about cell towers, so this object
    may be overly simplistic. It expects a central azimuth when created. 
    Based on this central azimuth, it creates a random field of view between
    60 and 120.
    '''
    def __init__(self, centralAngle):
        # generate random field of view between 60 and 120 degrees
        self.fov = random.randint(60, 120)
        
        # get azimuths
        self.azimuth1 = self.getLowerAzimuth(centralAngle)
        self.azimuth2 = self.getUpperAzimuth(centralAngle)
        
    # calculate the lower azimuth
    def getLowerAzimuth(self, centralAzimuth):
        
        # subtract half the field of view from the central angle
        azimuth1 = centralAzimuth - (self.fov / 2)
        
        # if the azimuth is less than zero
        if azimuth1 < 0:
            
            # add 360 degrees
            azimuth1 = azimuth1 + 360
            
        return azimuth1       
        
    def getUpperAzimuth(self, centralAzimuth):
        
        # add one half of the field of view to the central angle
        azimuth2 = centralAzimuth + (self.fov / 2)
        
        # if the azimuth is greater than 360
        if azimuth2 > 360:
            
            # subtract 360 degrees
            azimuth2 = azimuth2 - 360
            
        return azimuth2

class Tower(object):
    '''
    A cell tower with panels. When instantiated, it reads the carrier, height
    and geometry from the existing object. The range is calculated in
    assumed meters for ground units, eight miles for Verizon (CMDA) and four
    miles for everything else (GSM). Three randomly directed panels with 60 
    degree field of views are added to the tower.
    '''
    # only compile regular expression once to save memory
    regEx = re.compile('verizon', re.IGNORECASE)
    
    def __init__(self, licensee, height, geometry):
        self.licensee = licensee
        self.band = self.getBand()
        self.height = height
        self.range = self.getRange()
        self.panels = self.getRandomPanels()
        self.geometry = geometry
        
    def getRange(self):
        '''
        Range based on carrier and assumed band/frequency. 
        '''
        # assuming CDMA range for Verizon and all else GSM
        if re.search(self.regEx, self.licensee):
            miles = g_cdmaDist
        else:
            miles = g_gsmDist
            
        # calculating in meters
        conversionFactor = 1609.34
        
        # return range
        return miles * conversionFactor
    
    def getBand(self):
        '''
        Similar to getRange, but for settting band property.
        '''
        # assuming CDMA range for Verizon and all else GSM
        if re.search(self.regEx, self.licensee):
            band = 'CDMA'
        else:
            band = 'GSM'
            
        # return band
        return band
    
    def getRandomPanels(self):
        '''
        Generate random panels.
        '''
        # list to hold panel objects
        panels = []
        
        # generate the first panel's center azimuth to randomly be somewhere between 0 and 120
        firstazimuth = random.randint(0, 120)
        
        # add panels, each another 120 degrees past the previous
        for panelNum in range(0, 3, 1):
            azimuth = firstazimuth + (panelNum * 120)
            panels.append(Panel(azimuth))
            
        # return panels
        return panels
    
class Towers(list):
    '''
    All the towers combined as a Python list object instance with two added methods
    for loading and exporting cell phone towers. 
    '''
    # set desired spatial reference, USA_Contiguous_Equidistant_Conic
    spatialRef = arcpy.SpatialReference(102005)
    
    def load(self, inputFeatureClass):
        '''
        Add towers from the input feature class into list.
        '''
        # attribute field names from FCC cellular feature class
        licenseeField = 'LICENSEE'
        heightField = 'SUPSTRUC'
        
        # create projected dataset for geoprocessing, input is only NAD83
        cellTowers = arcpy.management.Project(inputFeatureClass, 
                                              os.path.join(arcpy.env.scratchGDB, 'cellTowers'), 
                                              arcpy.SpatialReference(102005)).getOutput(0)
        
        # fields to be loaded
        fields = [licenseeField, heightField, 'SHAPE@']
        
        # use search cursor to iterate input features
        with arcpy.da.SearchCursor(cellTowers, fields) as cursor:
            
            # for every row in the input feature class
            for row in cursor:
                
                # append a tower object onto the list
                self.append(Tower(row[0], row[1], row[2]))
                
    def export(self, outputFeatureClass):
        '''
        Export the fully attributed towers to a target feature class in Web Mercator
        Auxiliary Sphere to be more compatible with the online basemaps and possibly
        for inclusion in web applications.
        '''
        # create the feature class
        arcpy.management.CreateFeatureclass(out_path = os.path.dirname(outputFeatureClass),
                                            out_name = os.path.basename(outputFeatureClass),
                                            geometry_type = 'POINT',
                                            spatial_reference = self.spatialRef)
        
        # add fields to output feature class
        def addField(fieldName, fieldType):
            arcpy.management.AddField(outputFeatureClass, fieldName, fieldType)
        addField('LICENSEE', 'TEXT')
        addField('BAND', 'TEXT')
        addField('OFFSETA', 'DOUBLE')
        addField('AZIMUTH1', 'SHORT')
        addField('AZIMUTH2', 'SHORT')
        addField('RADIUS2', 'DOUBLE')
        
        # field list for cursor
        fields = ['LICENSEE', 'BAND', 'OFFSETA', 'AZIMUTH1', 'AZIMUTH2', 'RADIUS2', 'SHAPE@']
        
        # use update cursor to populate the fields
        with arcpy.da.InsertCursor(outputFeatureClass, fields) as cursor:
            
            # for every tower
            for tower in self:
                
                # for every panel on the tower
                for panel in tower.panels:
                    
                    # add a point
                    cursor.insertRow((tower.licensee,
                                      tower.band,
                                      tower.height,
                                      panel.azimuth1, 
                                      panel.azimuth2,
                                      tower.range, 
                                      tower.geometry))

def parameter(displayName, name, datatype, defaultValue=None,
    parameterType=None, direction=None):
    '''
    The parameter implementation makes it a little difficult to quickly
    create parameters with defaults. This method prepopulates the paramaeterType
    and direction parameters and leaves the setting a default value for the
    newly created parameter as optional. The displayName, name and datatype are
    the only required inputs.
    '''
    # create parameter with a few defaults
    param = arcpy.Parameter(
        displayName = displayName,
        name = name,
        datatype = datatype,
        parameterType = 'Required',
        direction = 'Input')

    # set new parameter to a default value
    param.value = defaultValue

    # return the complete parameter object
    return param

class Toolbox(object):
    def __init__(self):
        '''
        Define the toolbox properties here. Do not change the name of this
        class. ArcGIS locates this class by name. It will not be able to find
        the toolbox and your toolbox will not work if you modify this.
        '''
        self.label = 'FCC Cell Tower Viewshed'
        self.alias = 'FCC Cell Tower Viewshed'

        # List of tool classes associated with this toolbox
        self.tools = [getTowersTool, generateRandomPanelsTool]


class getTowersTool(object):
    '''
    Get Towers downloads and saves the cell tower data from the FCC into a
    geodatabase located in the same directory as where the script is saved. 
    '''
    def __init__(self):
        '''
        Define the tool class attributes, including your tool parameters.
        '''
        self.label = 'Get Towers'
        self.canRunInBackground = True

        self.parameters = [parameter('FCC Cell Tower Download URL',
                           'downloadUrl', 
                           'GPString', 
                           'http://wireless.fcc.gov/geographic/data/db/cellular06142012.zip'),
                           ]

    def getParameterInfo(self):
        return self.parameters

    def isLicensed(self):
        return True

    def updateParameters(self, parameters):
        return

    def updateMessages(self, parameters):
        return

    def execute(self, parameters, messages):
        # import modules specific to this tool
        import urllib
        import zipfile        
        
        # assign retrieved path to variable
        url = parameters[0].valueAsText
        arcpy.AddMessage(url)
        
        # if geodatabase does not already exist, create it
        if not arcpy.Exists(os.path.join(localDir, 'data.gdb')):
            arcpy.management.CreateFileGDB(localDir, 'data.gdb')
            
        # download zipped archive and create zipfile object instance
        zip = zipfile.ZipFile(urllib.urlretrieve(url)[0])
        
        # extract shapefile to scratch folder
        zip.extractall(arcpy.env.scratchFolder)
        
        # convert shapefile to gdb feature class
        arcpy.conversion.FeatureClassToFeatureClass(
            os.path.join(arcpy.env.scratchFolder, 'cellular.shp'), 
            os.path.join(localDir, 'data.gdb'), 
            cellTowersFc)
        
        return

class generateRandomPanelsTool(object):
    '''
    Using the FCC tower data as input, generate random panels for
    analysis. 
    '''
    def __init__(self):
        '''
        Define the tool class attributes, including your tool parameters.
        '''
        self.label = 'Generate Random Panels'
        self.canRunInBackground = True

        self.parameters = [parameter(displayName = 'Input FCC Cell Tower Feature Class', 
                                     name = 'inputFccTowers', 
                                     datatype = 'DEFeatureClass', 
                                     defaultValue= os.path.join(localDir, 'data.gdb', cellTowersFc)),
                           parameter(displayName = 'Output Geodatabase',
                                     name = 'outputGeodatabase',
                                     datatype = 'DEWorkspace',
                                     defaultValue = os.path.join(localDir, 'data.gdb')),
                           parameter(displayName = 'Output Panels Feature Class Name',
                                     name = 'outputPanelsFcName',
                                     datatype = 'GPString',
                                     defaultValue =  'panels'),
                           ]

    def getParameterInfo(self):
        '''
        Apply filters to input parameters.
        '''
        self.parameters[0].filter.list = ['Point']
        self.parameters[1].filter.list = ['Local Database', 'Remote Database']
                
        return self.parameters

    def isLicensed(self):
        return True

    def updateParameters(self, parameters):
        return

    def updateMessages(self, parameters):
        return

    def execute(self, parameters, messages):
        # create towers object instance
        towers = Towers()
        
        # load towers
        towers.load(parameters[0].valueAsText)
        
        # export towers
        towers.export(os.path.join(parameters[1].valueAsText, 
                                   parameters[2].valueAsText))
        
        return
